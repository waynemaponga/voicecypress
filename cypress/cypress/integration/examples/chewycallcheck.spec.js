
import SockJS from 'sockjs-client';
var Stomp = require("stompjs");


it('should able to conect to the sockect', () => {

  // cy
  // .request('POST', 'http://channel-processor-insecure-omni-channels-prod.app.prod-c.ocp.absa.co.za/api/channel-processor/login', {  requestType: "LOGIN_REQ",
  //   originatingSystem: "string",
  //   userId: "ablmayk",
  //   agentPin: "1234",
  //   channel: "string",
  //   stationId: "5016839"})
  // .then((response) => {
  //     console.log(response.body)
  //   // response.body is automatically serialized into JSON
  //   expect(response.body.agentId).equal('9011442')

  //   expect(response.body.errors).equal(false)
  //   var agentId = response.body.agentId;

  const socket = new SockJS('https://channel-processor-default-omni-channels-prod.app.prod-c.ocp.absa.co.za/socket.io/chewy');
  var stompClient = Stomp.over(socket);
  stompClient.connect({}, function (frame) {
    stompClient.subscribe('/queue/reply-9011442', (response) => {
 
      const newData = JSON.stringify(response.body); 
      const resJson = JSON.parse(newData);
      const conn = JSON.parse(resJson);
      const agentStatus= conn.agentStatus;
      if(agentStatus == null){
        console.log('did not recieved  agent status')
      }else{
        console.log(' recieved  agent status'+agentStatus )
      }

      if (conn.eventType === 'connectionEstablished') {
        console.log(' recieved  connectionEstablished event ')
        }
        if (conn.eventType === 'connectionDisconnected') {
          console.log(' recieved  connectionDisconnected event ')
          }

    });
  },
    error => {
  
        console.log('failed to connect to the sockect');
   


  })
})
it('should able to Login', () => {

  cy
  .request('POST', 'http://channel-processor-insecure-omni-channels-prod.app.prod-c.ocp.absa.co.za/api/channel-processor/login', {  requestType: "LOGIN_REQ",
    originatingSystem: "string",
    userId: "ablmayk",
    agentPin: "1234",
    channel: "string",
    stationId: "5016839"})
  .then((response) => {
      console.log(response.body)
    // response.body is automatically serialized into JSON
    expect(response.body.agentId).equal('9011442')
    expect(response.body.errors).equal(false)
    var agentId = response.body.agentId;
  })
})

it('should able to make a call', () => {

    cy
    .request('POST', 'http://channel-processor-insecure-omni-channels-prod.app.prod-c.ocp.absa.co.za/api/channel-processor/create-contact', {  requestType:"CREATE_CONTACT_REQ",
    originatingSystem:"string",
    userId:"ablmayk",
    channel:"string",
    agentId:"9011442",
    destinationId:"0822106860",
    stationId:"5016839",
    attachData:"chewy outbound"})
    .then((response) => {
   //response.body is automatically serialized into JSON
       expect(response.body.agentId).equal('9011442')
      expect(response.body.requestType).equal('CREATE_CONTACT_RESP')
      / expect(response.body.errors).equal(false)
    })
  
  
})