
var schema = {bdpchewyrouter: false,
startTime :"",
duaration :"",
channelprocessor:'2',
uiconfig:'2',
eventlistener:'2',
eventprocessor:'2',
serviceintegrator:'2',
  keycloakUrl: '2',
  channelProcessorApi: '2',
  sockectURL: '2',
  keycloakSecret: '2',
  xds :'2',
}

  it('It should check Health of PROD  bdpchewyrouter', () => {
    schema.startTime = Date.now();
    console.log("starting timer...");
    // https://on.cypress.io/request
    cy.request('http://omni-bdpchewyrouter-default-omni-channels-prod.app.prod-c.ocp.absa.co.za/actuator/health')
      .should((response) => {
        //console.log(response.body);
         const status = response.status;

        expect(response.status).to.eq(200)
        expect(response).to.have.property('status')
           if(status == '200'){
          schema.bdpchewyrouter = '20';
           }
           
      })
    
  })
   

  it('It should check Health of PROD  channel-processor', () => {
    // https://on.cypress.io/request
    cy.request('http://channel-processor-insecure-omni-channels-prod.app.prod-c.ocp.absa.co.za/actuator/health')
      .should((response) => {
        const status = response.status;
        expect(response.status).to.eq(200)
        expect(response).to.have.property('status')
        if(status == '200'){
          schema.channelprocessor =  '20';
           }
      })
  })
   
  it('It should check Health of PROD  chewy-ui-config', () => {
    // https://on.cypress.io/request
    cy.request('https://chewy-ui-config-server-secure-default-omni-channels-prod.app.prod-c.ocp.absa.co.za/actuator/health')
      .should((response) => {
        const status = response.status;
        expect(response.status).to.eq(200)
        expect(response).to.have.property('status')
        if(status == '200'){
          schema.uiconfig = '20';
           }
      })
  })
     
  it('It should check Health of PROD  chewy config settings', () => {
    // https://on.cypress.io/request
    cy.request('https://chewy-ui-config-server-secure-default-omni-channels-prod.app.prod-c.ocp.absa.co.za/environment')
      .should((response) => {
        expect(response.status).to.eq(200)
       
        expect(response.body.api.channelProcessorApi).to.eq('https://channel-processor-default-omni-channels-prod.app.prod-c.ocp.absa.co.za')
        expect(response.body.api.keycloakSecret).to.eq('2e0149a4-0716-4680-804b-f1a421ec2c56')
        expect(response.body.api.keycloakUrl).to.eq('https://sso-default-omni-channels-prod.app.prod-c.ocp.absa.co.za/auth/realms/chewy/protocol/openid-connect/token')
        expect(response.body.api.sockectURL).to.eq('https://channel-processor-default-omni-channels-prod.app.prod-c.ocp.absa.co.za/socket.io/chewy')

        if(expect(response.body.api.keycloakUrl).to.eq('https://sso-default-omni-channels-prod.app.prod-c.ocp.absa.co.za/auth/realms/chewy/protocol/openid-connect/token')){
         schema.keycloakUrl = '20';
        }
        if(expect(response.body.api.channelProcessorApi).to.eq('https://channel-processor-default-omni-channels-prod.app.prod-c.ocp.absa.co.za')){
          schema.channelProcessorApi = '20';
         }
         if(expect(response.body.api.keycloakSecret).to.eq('2e0149a4-0716-4680-804b-f1a421ec2c56')){
          schema.keycloakSecret = '20';
         }
         if(expect(response.body.api.sockectURL).to.eq('https://channel-processor-default-omni-channels-prod.app.prod-c.ocp.absa.co.za/socket.io/chewy')){
          schema.sockectURL = '20';
         }
          if(response.body.verification){
            schema.xds = '20';
          }else{
            schema.xds = "2";
          }


       // expect(response).to.have.property('status')
   
      })
  })
  it('It should check Health of PROD event-listener', () => {
    // https://on.cypress.io/request
    cy.request('http://event-listener-default-omni-channels-prod.app.prod-c.ocp.absa.co.za/actuator/health')
      .should((response) => {
        const status = response.status;
        expect(response.status).to.eq(200)
        expect(response).to.have.property('status')
        if(status == '200'){
          schema.eventlistener = '20';
           }
      })
  })
  it('It should check Health of PROD  event-processor', () => {
    // https://on.cypress.io/request
    cy.request('http://event-processor-default-omni-channels-uat.app.nonprod.ocp.absa.co.za/actuator/health')
      .should((response) => {
        const status = response.status;
        expect(response.status).to.eq(200)
        expect(response).to.have.property('status')
        if(status == '200'){
          schema.eventprocessor = '20';
           }
      })
  })

  it('It should check Health of PROD service-integrator', () => {
    // https://on.cypress.io/request
    cy.request('http://service-integrator-default-omni-channels-prod.app.prod-c.ocp.absa.co.za/actuator/health')
      .should((response) => {
        const status = response.status;
        expect(response.status).to.eq(200)
        expect(response).to.have.property('status')
        if(status == '200'){
          schema.serviceintegrator = '20';
           }
      
      })
     // const resultsobject =  JSON.parse(schema);
      //const results =  JSON.parse(schema);
      console.log(JSON.stringify(schema))
          
  })

  it('should able save report', () => {

    const  report = schema
    cy
    .request('POST', 'http://localhost:3000/reports', {
      bdpchewyrouter:report.bdpchewyrouter,
      startTime:report.startTime,
      duaration:"",
      channelprocessor:report.channelprocessor,
      uiconfig:report.uiconfig,
      eventlistener:report.eventlistener,
      eventprocessor:report.eventprocessor,
      serviceintegrator:report.serviceintegrator,
      keycloakUrl:report.keycloakUrl,
      channelProcessorApi:report.channelProcessorApi,
      sockectURL:report.sockectURL,
      keycloakSecret:report.keycloakSecret,
      xds:report.xds
    })
    .then((response) => {
     // expect(response.message).to.have.property('message')
    })
  })
